# Oracle Logger

## Getting started

This is a minimalistic logger for the Oracle Database. It is intended to be used in PL/SQL stored procedures in order to collect information.

## Installation

The ```logger_create.sql``` will install the logger package in the current schema. 
Where to install the package? There are two options.
- You may decide to install privately in any schema 
- Or you may prefer a more centralized approach installing the package in a specific schema. You will need to grant execution with definer rights in order to store logs.

## Usage

Using the logger is straightforward.

```
begin
	logger.load_active_configuration;
    logger.start_logging;
	logger.log(logger.ERROR, 'Oh no, an error occured!');
    logger.stop_logging;
end;
```

For further details please refer to API documentation in [Api/logger.html](Api/logger.html).

## Project status

I wrote this code on Oracle 8.1.7. I have not mantained it from since.

## License

Code is released under the MIT license.
