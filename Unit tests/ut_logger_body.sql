create or replace package body ut_logger is
	FILE_DIR constant varchar2(20) := '/tmp';
	FILE_DEST constant varchar2(20) := 'logger.out';
	TABLE_DEST constant varchar2(20) := 'logger_msg';
	
	FILE_CONF constant varchar2(20) := 'logger_ut_file_conf';
	TABLE_CONF constant varchar2(20) := 'logger_ut_table_conf';
	XXX_CONF constant varchar2(20) := 'logger_ut_xxx';
	
	procedure ut_setup is
	begin
		-- configure utplsql
		utconfig.registertest(true);
		utconfig.autocompile(false);
		utconfig.showfailuresonly(true);
		
		-- prepare logger configurations
		delete logger_conf 
		where conf in (FILE_CONF, TABLE_CONF, XXX_CONF);
		insert into logger_conf (conf, target, destination, destination2, severity)
		values (FILE_CONF, logger.TO_FILE, FILE_DIR, FILE_DEST, logger.INFO);
		insert into logger_conf (conf, target, destination, severity)
		values (TABLE_CONF, logger.TO_TABLE, TABLE_DEST, logger.TRACE);
		commit;
		
		-- build test case
		utplsql.addtest('ut_load_configuration');
		utplsql.addtest('ut_set_active_configuration');
		utplsql.addtest('ut_load_active_configuration');
		utplsql.addtest('ut_store_configuration');
		utplsql.addtest('ut_log');
		utplsql.addtest('ut_stop_logging');
		utplsql.addtest('ut_no_log_table');
		utplsql.addtest('ut_log_to_file');
		utplsql.addtest('set_severity');
		utplsql.addtest('set_destination');
		utplsql.addtest('set_target');
	end;
	
	procedure ut_teardown is
	begin
		null;
	end;
	
	/*
		deve settare correttamente i valori per target, destination, severity.
		solleva NO_DATA_FOUND se la configurazione non esiste.
	*/
	procedure ut_load_configuration is
	begin
		logger.load_configuration(TABLE_CONF);
		utassert.eq('logger target', logger.get_target, logger.TO_TABLE);
		utassert.eq('logger destination', logger.get_destination, TABLE_DEST);
		utassert.eq('logger severity', logger.get_severity, logger.TRACE);
		utassert.throws('configuration does not exists (gets no_data_found)',
		                'logger.load_configuration('''||XXX_CONF||''');',
		                'NO_DATA_FOUND');
	end;
	
	/*
		deve settare il flag active sulla configurazione indicata.
	*/
	procedure ut_set_active_configuration is
		myactive number;
	begin
		logger.set_active_configuration(TABLE_CONF);
		select active into myactive from logger_conf where conf = TABLE_CONF;
		utassert.this('logger active configuration', myactive != 0);
	end;
	
	/*
		deve caricare la configurazione attiva.
	*/
	procedure ut_load_active_configuration is
	begin
		logger.load_configuration(FILE_CONF);
		logger.set_active_configuration(TABLE_CONF);
		logger.load_active_configuration;
		utassert.eq('logger target', logger.get_target, logger.TO_TABLE);
		utassert.eq('logger destination', logger.get_destination, TABLE_DEST);
		utassert.eq('logger severity', logger.get_severity, logger.TRACE);
	end;
	
	/*
		memorizza la configurazione attiva.
		se la configurazione esiste solleva una violazione di pk.
	*/
	procedure ut_store_configuration is
		stored number;
	begin
		delete logger_conf where conf = 'uttest';
		commit;
		logger.store_configuration('uttest');
		select count(*) into stored from logger_conf where conf = 'uttest';
		utassert.eq('stored configuration', stored, 1);
		utassert.throws('store configuration 2 times (raises dup_val_on_index)', 
		                'logger.store_configuration(''uttest'');',
		                'DUP_VAL_ON_INDEX');
	end;
	
	/*
		verifica che il log sia effettuto se il livello di severita'
		del logger e' maggiore di quello indicato dall'operazione.
		verifica che il log non sia effettuato quando la severita' del
		logger e' superiore a quella del comando di log.
	*/
	procedure ut_log is
		i integer;
	begin
		execute immediate 'delete '||TABLE_DEST;
		logger.load_configuration(TABLE_CONF);
		logger.log(logger.ERROR, 'error level log test');
		logger.log(logger.INFO, 'info level log test');
		logger.log(logger.TRACE, 'trace level log test');
		
		execute immediate 'select count(*)from '||TABLE_DEST into i;
		utassert.eq('logged events', i, 3);
		
		logger.set_severity(logger.INFO);
		logger.log(logger.ERROR, 'error level log test');
		logger.log(logger.INFO, 'info level log test');
		logger.log(logger.TRACE, 'trace level log test'); -- non loggato
		
		execute immediate 'select count(*)from '||TABLE_DEST into i;
		utassert.eq('logged events', i, 5);
	end;
	
	/*
		verifica che i log non vengano registrati quando il logger 
		e' sospeso.
	*/
	procedure ut_stop_logging is
		i integer;
	begin
		execute immediate 'delete '||TABLE_DEST;
		logger.load_configuration(TABLE_CONF);
		logger.log(logger.ERROR, 'error level log test 1');
		logger.stop_logging;
		logger.log(logger.ERROR, 'error level log test 2');
		logger.start_logging;
		logger.log(logger.ERROR, 'error level log test 3');
		
		execute immediate 'select count(*)from '||TABLE_DEST into i;
		utassert.eq('logged events', i, 2);
	end;
	
	/*
		la procedura non intercetta gli errori sollevati da utl_file e
		li lascia gestire al programmatore.
		richiede che /tmp e / siano definite in utl_file_dir.
	*/
	procedure ut_log_to_file is
	begin
		logger.load_configuration(FILE_CONF);
		logger.set_destination('tmp');
		logger.set_destination2('logger.out');
		utassert.throws('not absolute filename (gets invalid_path)',
		                'logger.log(''not absolute filename'');', 
		                'utl_file.invalid_path');
				
		logger.set_destination('/tmp');
		logger.set_destination2(null);
		utassert.throws('incomplete filename (gets invalid_path)',
		                'logger.log(''incomplete filename'');',
		                'utl_file.invalid_path');
				
		logger.set_destination('/tmp');
		logger.set_destination2('logger.out');
		utassert.this('correct filename', SQLCODE = 0);
		
		logger.set_destination('/');
		utassert.throws('no write permission (gets invalid_operation)',
		                'logger.log(''no write permission'');',
		                'utl_file.invalid_operation');
	end;
	
	/*
		il log su tabella, se la destinazione non esiste, solleva
		l'eccezione LOG_TABLE_NOT_FOUND.
	*/
	procedure ut_no_log_table is
	begin
		logger.load_configuration(TABLE_CONF);
		logger.set_destination('does_no_exists');
		utassert.throws('table logger exceptions (gets LOG_TABLE_NOT_FOUND)',
		                'logger.log(''no log table test'');',
		                'LOGGER.LOG_TABLE_NOT_FOUND');
	end;
	
	procedure set_target is
	begin
		logger.load_configuration(TABLE_CONF);
		utassert.throws('null target (throws invalid_target)', 
		                'logger.set_target(null);', 'logger.invalid_target');
		utassert.throws('invalid target (throws invalid_target)',
		                'logger.set_target(-1);', 'logger.invalid_target');
	end;
	
	procedure set_destination is
	begin
		logger.load_configuration(TABLE_CONF);
		utassert.throws('null destination (throws invalid_destination)', 
		                'logger.set_destination(null);',
		                'logger.invalid_destination');
	end;

	procedure set_severity is
	begin
		logger.load_configuration(TABLE_CONF);
		utassert.throws('null severity (throws invalid_severity)', 
		                'logger.set_severity(null);', 
		                'logger.invalid_severity');
		utassert.throws('invalid target (throws invalid_severity)',
		                'logger.set_severity(-1);',
		                'logger.invalid_severity');
	end;
	
end;
/
show errors
