create or replace package ut_logger is
	procedure ut_setup;
	procedure ut_teardown;
	
	procedure ut_load_configuration;
	procedure ut_set_active_configuration;
	procedure ut_load_active_configuration;
	procedure ut_store_configuration;
	procedure ut_log;
	procedure ut_stop_logging;
	procedure ut_no_log_table;
	procedure ut_log_to_file;
	procedure set_target;
	procedure set_destination;
	procedure set_severity;
end;
/
show errors
