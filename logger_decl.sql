create or replace package logger
/**
 * A simple log framework for PL/SQL. The framework is oriented to tracing
 * plsql application and to store application error for further investigation.
 * There are two methods <i>(targets)</i> of storing logs.
 * Logs can be stored on a table or on a text log file on the database server
 * using <i>utl_file</i>. Table logging may be preferrable to manage logs
 * without external tools.
 */
is
	/**
	 * Log information will be stored in a text file on the database server.
	 */
	TO_FILE constant number := 1;
	/**
	 * Log information will be stored in a table.
	 */
	TO_TABLE constant number := 0;
	
	/**
	 * Identifies the maximum threshold of detail; any information will
	 * be logged.
	 */
	TRACE constant number := 2;
	/**
	 * Identifies a medium thresold of detail, trace information will be
	 * discarded.
	 */
	INFO constant number := 1;
	/**
	 * The default thresold of detail, only errors will be logged.
	 */
	ERROR constant number := 0;
	
	-- exceptions
	LOG_TABLE_NOT_FOUND exception;
	INVALID_DESTINATION exception;
	INVALID_TARGET exception;
	INVALID_SEVERITY exception;
	

	/** Returns the logger package version. */
	function get_version return varchar2;
	
	/**
	 * Information passed to procedure <b>log()</b> will be stored according
	 * to the actual log policy.
	 */
	procedure start_logging;
	/**
	 * Log is suspended; no information will be stored, logging activity
	 * has to be enabled with <b>start_logging</b>.
	 */
	procedure stop_logging;
	
	/**
	 * Logs <i>text</i> with the desired severity; the log will be stored
	 * only if <i>severity</i> does not exceed the level of detail of the
	 * logger policy.
	 * Exception related to server file logging are defined and better
	 * explained in <i>utl_file</i> documentation.
	 *
	 * @throws LOGGER_TABLE_NOT_FOUND When logger table cannot be found.
	 * @throws INVALID_PATH When server log file path is incorrect.
	 * @throws INVALID_OPERATION Server file logging error.
	 * @throws WRITE_ERROR An error occured while writing the log on the
	 *                     server log file, maybe the file is read only.
	 */
	procedure log(severity number, text varchar2);
	/**
	 * Logs <i>text</i> with ERROR severity (will always be logged).
	 * Exception related to server file logging are defined and better
	 * explained in <i>utl_file</i> documentation.
	 *
	 * @throws LOGGER_TABLE_NOT_FOUND When logger table cannot be found.
	 * @throws INVALID_PATH When server log file path is incorrect.
	 * @throws INVALID_OPERATION Server file logging error.
	 * @throws WRITE_ERROR An error occured while writing the log on the
	 *                     server log file, maybe the file is read only.
	 */
	procedure log(text varchar2);
	
	/**
	 * Set the log destination. Default destination is table logger_msg.
	 * Refer to <i>utl_file</i> documentation for further detail on server
	 * file logging.
	 *
	 * @param dest If target is TABLE <i>dest</i> is the name of the table
	 *             which will store logs; if target is FILE <i>dest</i> is
	 *             the absolute basename (path without file name and 
	 *             trailing path separator) of the log file. The log file
	 *             resides on the database server.
	 * @throws INVALID_DESTINATION If target is null.
	 */
	procedure set_destination(dest varchar2);
	/**
	 * Describes the log destination, which will be the table to which you
	 * are logging to or the absolute basename of the log file.
	 */
	function get_destination return varchar2;
	
	/**
	 * Set the log file name for FILE logging; this parameter is ignored
	 * when logging to table.
	 *
	 * @param dest The filename without preceding path separator. The log
	 *             file resides on the database server.
	 */
	procedure set_destination2(dest varchar2);
	/** Describes the log file name. */
	function get_destination2 return varchar2;
	
	/**
	 * Set the log target which currently may be FILE or TABLE.
	 * The default target is TABLE.
	 *
	 * @throws INVALID_TARGET If target is unknown.
	 */
	procedure set_target(target number);
	/**
	 * Describes the log target.
	 */
	function get_target return number;
	
	/**
	 * Sets the level of detail of information which will be logged.
	 * Logs with higher detail will be discarded.
	 *
	 * @param severity The desired level of detail.
	 * @throws SEVERITY When severity is unknown.
	 */
	procedure set_severity(severity number);
	/**
	 * Describes the log severity.
	 */
	function get_severity return number;
	
	/**
	 * Load the <i>conf</i> log policy.
	 *
	 * @throwns NO_DATA_FOUND If <i>conf</i> log policy cannot be found.
	 */
	procedure load_configuration(conf varchar2);
	/**
	 * Load the active log policy.
	 *
	 * @throwns NO_DATA_FOUND If there is not an active policy.
	 */
	procedure load_active_configuration;
	/**
	 * Store the actual policy and bind it to the <i>conf</i> identifier.
	 *
	 * @throwns DUP_VAL_ON_INDEX If the policy is already defined.
	 */
	procedure store_configuration(conf varchar2);
	
	/**
	 * Mark the policy <i>conf</i> as the active policy.
	 * Because the previous policy is disabled, if the new policy
	 * dos not exists there will be no active configuration and
	 * logger will use the default policy.
	 */
	 
	procedure set_active_configuration(conf varchar2);
	/**
	 * Restore the default logger policy which is logging only errors
	 * to table <i>logger_msg</i>.
	 */
	procedure reset_configuration;
end;
/
