create table logger_msg
(
	sessionid number not null,
	time_stamp date not null,
	severity varchar2(20) not null,
	text varchar2(4000) not null,
	seq number not null
);

create sequence sq_logger_msg;

create or replace trigger tr_logger_msg
before insert on logger_msg
referencing old as old new as new
for each row
begin
	select sq_logger_msg.nextval into :new.seq from dual;
end;
/

create table logger_conf
(
	conf varchar2(20) not null,
	target number not null,
	destination varchar2(200) not null,
	destination2 varchar2(100),
	severity number not null,
	active number default 0 not null,
	constraint pk_logger_conf primary key (conf)
);

create or replace view logger_confx as
select conf, decode(target, 0, 'TO_TABLE', 1, 'TO_FILE') target, 
	decode(severity, 0, 'REPORT_ERRORS',
	                 1, 'REPORT_INFO',
	                 2, 'REPORT_ALL') severity,
	destination,
	decode(active, 0, 'NO', 'YES') active
from logger_conf;
