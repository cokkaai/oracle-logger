create or replace package body logger is
	VERSION constant varchar2(20) := 'logger 1.0';
	DEFAULT_TABLE_DEST constant varchar2(20) := 'logger_msg';
	MAX_TEXT_LENGTH constant integer := 4000;
	destination logger_conf.destination%TYPE;
	destination2 logger_conf.destination2%TYPE;
	target logger_conf.target%TYPE;
	severity logger_conf.severity%TYPE;
	logging number := 1;
	
	procedure log_to_file(severity number, text varchar2);
	procedure log_to_table(severity number, text varchar2);
	function describe_severity(sev number) return varchar2;
	function build_header(sev number) return varchar2;
	

	procedure set_destination(dest varchar2) is
	begin
		if dest is null then
			raise logger.INVALID_DESTINATION;
		end if;
		destination := dest;
	end;
	
	function get_destination return varchar2 is
	begin
		return destination;
	end;
	
	procedure set_destination2(dest varchar2) is
	begin
		destination2 := dest;
	end;
	
	function get_destination2 return varchar2 is
	begin
		return destination2;
	end;
	
	procedure set_target(target number) is
	begin
		if set_target.target != logger.TO_FILE 
			and set_target.target != logger.TO_TABLE
		then
			raise logger.INVALID_TARGET;
		elsif set_target.target is null then
			raise logger.INVALID_TARGET;
		end if;
		logger.target := set_target.target;
	end;
	
	function get_target return number is
	begin
		return target;
	end;

	procedure set_severity(severity number) is
	begin
		if severity < ERROR or severity > TRACE then
			raise logger.INVALID_SEVERITY;
		elsif severity is null then
			raise logger.INVALID_SEVERITY;
		else
			logger.severity := set_severity.severity;
		end if;
	end;
	
	function get_severity return number is
	begin
		return severity;
	end;

	procedure start_logging is
	begin
		logging := 1;
	end;
	
	procedure stop_logging is
	begin
		logging := 0;
	end;
	
	procedure set_active_configuration(conf varchar2) is
		pragma autonomous_transaction;
	begin
		update logger_conf set active = 0;
		update logger_conf set active = 1
		where conf = set_active_configuration.conf;
		commit;
	end;
	
	procedure store_configuration(conf varchar2) is
		pragma autonomous_transaction;
	begin
		if target = TO_TABLE then
			logger.destination2 := null;
		end if;
		insert into logger_conf(conf, target, destination, destination2, severity)
		values (store_configuration.conf, logger.target, 
		         logger.destination, logger.destination2, logger.severity);
		commit;
	end;
	
	procedure load_configuration(conf varchar2) is
	begin
		select target, destination, destination2, severity
		into logger.target, logger.destination, logger.destination2, logger.severity
		from logger_conf
		where conf = load_configuration.conf;
	end;
	
	procedure load_active_configuration is
	begin
		select target, destination, destination2, severity
		into logger.target, logger.destination, logger.destination2, logger.severity
		from logger_conf
		where active != 0;
	end;
	
	procedure reset_configuration is
	begin
		destination := DEFAULT_TABLE_DEST;
		destination2 := null;
		target := TO_TABLE;
		severity := ERROR;		
	end;
	
	procedure log(severity number, text varchar2) is
	begin
		if logging = 0 then 
			return; 
		end if;
		if log.severity is null then
			raise logger.INVALID_SEVERITY;
		elsif log.severity > logger.severity then 
			return;
		end if;
		if target = TO_FILE then
			log_to_file(severity, text);
		elsif target = TO_TABLE then
			log_to_table(severity, text);
		end if;
	end;
	
	procedure log(text varchar2) is
	begin
		log(ERROR, text);
	end;

	function build_header(sev number) return varchar2 is
		sid number;
	begin
		select userenv('sessionid') into sid from dual;
		return to_char(sysdate, 'dd-mm-yyyy hh24:mi:ss')
		       ||', sessionid = '||sid
		       ||', '||describe_severity(sev);
	end;
	
	function describe_severity(sev number) return varchar2 is
	begin
		if sev = ERROR then
			return 'error';
		elsif sev = INFO then
			return 'info';
		elsif sev = TRACE then
			return 'trace';
		else
			return 'unknown';
		end if;
	end;
	
	procedure log_to_file(severity number, text varchar2) is
		hnd utl_file.file_type;
		msg varchar2(4000);
		hdr varchar2(100);
	begin
		hdr := build_header(log_to_file.severity)||', ';
		msg := hdr||substr(text, 1, 4000 - length(hdr));
		-- dbms_output.put_line('msg: '||msg); 
		hnd := utl_file.fopen(destination, destination2, 'a');
		utl_file.put_line(hnd, msg);
		utl_file.fclose(hnd);
	end;
	
	procedure log_to_table(severity number, text varchar2) is
		pragma autonomous_transaction;
		stmt varchar2(200);
	begin
		stmt := 'insert into '||destination
		        ||' (sessionid, time_stamp, severity, text)'
		        ||' select userenv(''sessionid''), sysdate, :1, :2 from dual';
		execute immediate stmt using log_to_table.severity, text;
		commit;
	exception
		when others then
			if SQLCODE = -942 then -- table or view not found
				raise logger.LOG_TABLE_NOT_FOUND;
			else
				raise;
			end if;
	end;
	
	function get_version return varchar2 is
	begin
		return VERSION;
	end;
	
begin
	load_active_configuration;
exception
	when NO_DATA_FOUND then
		reset_configuration;
	when others then
		raise;
end;
/
